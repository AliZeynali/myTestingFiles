import os
import struct
import random
import sys
from pathlib import Path
from threading import Thread, Semaphore

MSS = 1480
Time = 0
originalSequenceNumber = random.randint(0, (2 ** 32) - 1)
windowSize = 1
lastWindowSize = 1
halfOfWindowSize = 1
windowPointer = 0
sendTime = []
numberOfACKs = []
data = []
header = []
senderPort = 0
receiverPort = 0
lock = Semaphore(1)
sampleRTT = 0
previousEstimatedRTT = 0
timeOut = False
tripleDuplicateAck = False
SYN = 0
FIN = 0
ACK = 0


def estimateRTT():
    global sampleRTT, previousEstimatedRTT
    newRTT = (1 - 0.125) * previousEstimatedRTT + 0.125 * sampleRTT
    previousEstimatedRTT = newRTT
    return newRTT

def sendWindow():
    lock.acquire()
    for i in range(windowPointer, windowPointer + windowSize):
        writeOnForwardNet(header[i] + data[i])
        sendTime[i] = Time

def checkTimeout():
    global windowSize, timeOut, halfOfWindowSize, lastWindowSize
    if Time - sendTime[windowPointer] > 2 * previousEstimatedRTT:
        halfOfWindowSize = int(windowSize / 2)
        if halfOfWindowSize < 1:
            halfOfWindowSize = 1
        windowSize = 1
        lastWindowSize = halfOfWindowSize
        header[windowPointer] = packHeader(getSequenceNumber(windowPointer + originalSequenceNumber + 1), getAcknowledgmentNumber(), getDataOffsetAndReservedAndNS()
                                           , getCEUAPRSF(1), getChecksum(packHeader(getSequenceNumber(windowPointer + originalSequenceNumber + 1), getAcknowledgmentNumber(), getDataOffsetAndReservedAndNS()
                                           , getCEUAPRSF(1), struct.pack("!H", 0), getUrgentPointer())), getUrgentPointer())
        timeOut = True

def checkTrippleDuplicatAck():
    global tripleDuplicateAck, windowSize, lastWindowSize
    if numberOfACKs[windowPointer] == 4:
        windowSize = int(windowSize / 2)
        lastWindowSize = windowSize
        tripleDuplicateAck = True


def time(senderTime):
    global Time, lock
    with open(senderTime) as file:
        print("time is open")
        while (True):
            time = file.read()
            if time.replace("\n", "") == "tick":
                Time += 1
                lock.release()
                checkTimeout()


def writeOnForwardNet(packet, file):
    l = len(packet)
    x0 = l % 256
    l = l // 256
    x1 = l % 256
    l = l // 256
    x2 = l % 256
    l = l // 256
    x3 = l
    if x3 >= 256:
        raise Exception('Very large buffer')

    file.write(bytes([x3, x2, x1, x0]))
    file.write(bytes(packet))
    file.flush()


def readFromSenderData(senderDataPipe):
    length = senderDataPipe.read(4)
    if len(length)>0:
        header = senderDataPipe.read(20)
        return header
    else:
        return


def getSequenceNumber(sequenceNumber):


    global originalSequenceNumber
    if SYN == 1:
        result = originalSequenceNumber
    else:
        result = sequenceNumber
    seq0 = result % (256 * 256)
    result = result // (256 * 256)
    seq1 = result % (256 * 256)

    return struct.pack("!HH", seq1, seq0)


def getAcknowledgmentNumber():  # in sender
    return struct.pack("!HH", 0, 0)


def getDataOffsetAndReservedAndNS():
    return struct.pack("!B", (5 << 4))


def getCEUAPRSF(CWR):
    if SYN:
        result = (ACK << 4) + (1 << 1)
    elif FIN:
        result = (ACK << 4) + 1
    elif CWR:
        result = (1 << 7)
    else:
        result = (ACK << 4)
    return struct.pack("!H", result)


def unpackCEUAPRSF(CEUAPRSF):
    CEUAPRSF = "{0:08b}".format(struct.unpack_from("!B", CEUAPRSF)[0])
    return int(CEUAPRSF[0]), int(CEUAPRSF[3]), int(CEUAPRSF[6]), int(CEUAPRSF[7])


def getChecksum(packetWithZero):
    byteList = []
    length = len(packetWithZero)
    index = 0
    if length % 2 == 1:
        packetWithZero += struct.pack("!B", 0)
        length += 1
    while True:
        if index >= length:
            break
        element = packetWithZero[index: index + 2]
        byteList.append(element)
        index += 2
    answer = 0
    for value in byteList:
        ############
        value = struct.unpack_from("!H", value)[0]
        answer += value
        if answer > 2 ** 16:
            answer = answer - 2 ** 16 + 1
    answer = 2 ** 16 - answer
    checkSum = struct.pack("!H", answer)
    # newPacket = pack[0:16] + checkSum + pack[18:]
    ###############
    return checkSum


# todo


def getUrgentPointer():
    return struct.pack("!H", 0)


def unpack(packet):
    return packet[0:20], packet[20:]


def unpackHeader(header):
    sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = \
        header[4:8], header[8:12], header[12], header[13], header[14:16], header[16:18]
    return sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum


def packHeader(seqNum, ackNum, dataOffsetAndReservedNS, CEUAPRSF, checksum, urgent):
    return struct.pack("!HH", senderPort,
                       receiverPort) + seqNum + ackNum + dataOffsetAndReservedNS + CEUAPRSF + struct.pack("!H", windowSize) + checksum + urgent


if __name__ == "__main__":

    if len(sys.argv) == 6:

        senderDataPipe = '../pipes/sender_{0}_data.pipe'.format(sys.argv[1])
        senderTimePipe = '../pipes/sender_{0}_time.pipe'.format(sys.argv[1])
        try:
            os.mkfifo(senderDataPipe)
            os.mkfifo(senderTimePipe)
        except Exception:
            print("error")
            exit(0)

        senderPort = int(sys.argv[1])
        receiverPort = int(sys.argv[2])
        initRTT = int(sys.argv[3])
        maxWindow = int(sys.argv[4])
        filePath = sys.argv[5]

        if not Path('../pipes/receiver_{0}_data.pipe'.format(receiverPort)).exists():
            print("sending host {0}: no receiving host {1} is available".format(senderPort, receiverPort))
            exit(0)

        tickThread = Thread(target=time, args=(senderTimePipe,))
        tickThread.start()

        forwardNet = open('../pipes/forwardnet_data.pipe', "wb")

        # send SYN

        SYN = 1

        writeOnForwardNet(packHeader(getSequenceNumber(0), getAcknowledgmentNumber(), getDataOffsetAndReservedAndNS(),
                                     getCEUAPRSF(0),
                                     getChecksum(packHeader(getSequenceNumber(0), getAcknowledgmentNumber(),
                                                            getDataOffsetAndReservedAndNS(),
                                                            getCEUAPRSF(0), struct.pack("!H", 0),
                                                            getUrgentPointer())), getUrgentPointer()), forwardNet)

        print("sending host {0}: is running...".format(senderPort))

        senderDataPipe = open(senderDataPipe, "rb")

        # receive SYN/ACK
        while True:
            SYNACKHeader = readFromSenderData(senderDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                SYNACKHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack , syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if syn and ack:
                    break

        # send ACK
        SYN = 0
        ACK = 1
        writeOnForwardNet(packHeader(getSequenceNumber(originalSequenceNumber + 1), getAcknowledgmentNumber(), getDataOffsetAndReservedAndNS(),
                                     getCEUAPRSF(0),
                                     getChecksum(packHeader(getSequenceNumber(originalSequenceNumber + 1), getAcknowledgmentNumber(),
                                                            getDataOffsetAndReservedAndNS(),
                                                            getCEUAPRSF(0), struct.pack("!H", 0),
                                                            getUrgentPointer())), getUrgentPointer()))
        originalSequenceNumber += 1

        with open(filePath, "rb") as file:
            dataLoad = file.read(MSS)
            while len(dataLoad) > 0:
                data.append(dataLoad)

        ACK = 0

        for i in range(data):
            header[i] = packHeader(getSequenceNumber(originalSequenceNumber + i + 1), getAcknowledgmentNumber(),
                       getDataOffsetAndReservedAndNS(),
                       getCEUAPRSF(0),
                       getChecksum(packHeader(getSequenceNumber(originalSequenceNumber + i + 1), getAcknowledgmentNumber(),
                                              getDataOffsetAndReservedAndNS(),
                                              getCEUAPRSF(0), struct.pack("!H", 0),
                                              getUrgentPointer())), getUrgentPointer())

        #read packet ACKs

        while True:
            header = readFromSenderData(senderDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                header)
            acknowledgmentNumber = struct.unpack("!HH", acknowledgmentNumber)
            ackNumber = acknowledgmentNumber[0] * 256 * 256 + acknowledgmentNumber[1]

            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if (ackNumber - originalSequenceNumber) == len(data) - 1:
                    break
                else:
                     numberOfACKs[ackNumber - originalSequenceNumber - 1] += 1

                     if timeOut:
                         if windowSize <= halfOfWindowSize or lastWindowSize <= 0:
                             sendWindow()
                             windowPointer += 1
                             windowSize += 1
                         elif lastWindowSize > 0:
                             sendWindow()
                             windowPointer += 1
                             lastWindowSize -= 1

                     elif tripleDuplicateAck:
                         if lastWindowSize > 0:
                            sendWindow()
                            windowPointer += 1
                            lastWindowSize -= 1
                         else:
                             sendWindow()
                             if windowSize < maxWindow:
                                windowSize += 1
                             windowPointer += 1

                     else:
                         windowPointer += 1
                         if windowSize < maxWindow:
                            windowSize += 1

                     if numberOfACKs[ackNumber - originalSequenceNumber -2] == 1:
                         sampleRTT = Time - sendTime[ackNumber - originalSequenceNumber - 2]
                         estimateRTT()

        # send FIN
        FIN = 1
        writeOnForwardNet(packHeader(getSequenceNumber(originalSequenceNumber + len(data)+1), getAcknowledgmentNumber(),
                                     getDataOffsetAndReservedAndNS(),
                                     getCEUAPRSF(0),
                                     getChecksum(packHeader(getSequenceNumber(originalSequenceNumber + len(data) + 1),
                                                            getAcknowledgmentNumber(),
                                                            getDataOffsetAndReservedAndNS(),
                                                            getCEUAPRSF(0), struct.pack("!H", 0),
                                                            getUrgentPointer())), getUrgentPointer()))



        # receive FIN/ACK
        while True:
            FINACKHeader = readFromSenderData(senderDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                SYNACKHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack , syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if fin and ack:
                    break

        #receive FIN
        while True:
            FINACKHeader = readFromSenderData(senderDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                SYNACKHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                                struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if fin:
                    break

        # send FIN/ACK
        FIN = 1
        ACK = 1
        writeOnForwardNet(
            packHeader(getSequenceNumber(originalSequenceNumber + len(data) + 2), getAcknowledgmentNumber(),
                        getDataOffsetAndReservedAndNS(),
                        getCEUAPRSF(0),
                        getChecksum(packHeader(getSequenceNumber(originalSequenceNumber + len(data) + 2),
                                                getAcknowledgmentNumber(),
                                                getDataOffsetAndReservedAndNS(),
                                                getCEUAPRSF(0), struct.pack("!H", 0),
                                                getUrgentPointer())), getUrgentPointer()))

        tickThread.join()
        print("sending host {0}: is terminated.".format(senderPort))
        exit(0)
