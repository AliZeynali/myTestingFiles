#!/usr/bin/env python3
import sys
from sys import argv
import struct
from threading import Thread


def send_tick(time_pipe):
    if not time_pipe:
        return
    time_pipe.write("tick\n")
    time_pipe.flush()


def send_data(receiver_data, data):
    if not receiver_data:
        print("No Receiver founded")
        exit(0)

    l = len(data)
    x0 = l % 256
    l = l // 256
    x1 = l % 256
    l = l // 256
    x2 = l % 256
    l = l // 256
    x3 = l
    if x3 >= 256:
        raise Exception('Very large buffer')

    receiver_data.write(bytes([x3, x2, x1, x0]))
    receiver_data.write(data)
    receiver_data.flush()


def send_read_file(reader, time_pipe, receiver):
    if not reader:
        print("not reader founded")
        exit(0)
    while True:
        data = reader.read(4)
        if len(data) > 0:
            ##############
            print("I found file")
            list = struct.unpack_from("!HH", data)
            mostValue = list[0]
            leastValue = list[1]
            length = mostValue * 256 * 256 + leastValue
            data = reader.read(length)
            send_handle_data(time_pipe, receiver, data)


def receive_read_file(reader, receiver):
    if not reader:
        print("not reader founded")
        exit(0)
    while True:
        data = reader.read(4)
        if len(data) > 0:
            ###############
            print("I found file")
            list = struct.unpack_from("!HH", data)
            mostValue = list[0]
            leastValue = list[1]
            length = mostValue * 256 * 256 + leastValue
            data = reader.read(length)
            receive_handle_data(receiver, data)


def send_handle_data(time_pipe, receiver_data, data):
    while True:
        if not receiver_data:
            continue
        else:
            ###############
            print("Give me order:   [ s#N   :   send and write N tick   ,   d#N   :   drop an write N tick ]")
            string = input()
            print(string)
            if string[0] != 's' and string[0] != 'd':
                continue
            tick_number = int(string[1:])
            for i in range(0, tick_number):
                send_tick(time_pipe)
            if string[0] == 's':
                send_data(receiver_data, data)
                return
            elif string[0] == 'd':
                return


def receive_handle_data(receiver_data, data):
    while True:
        if not receiver_data:
            continue
        else:
            print("Give me order    [ s : Send   ,   d   : drop ]")
            string = input()
            if string[0] == 's':
                send_data(receiver_data, data)
                return
            elif string[0] == 'd':
                return


if __name__ == "__main__":
    try:
        if len(argv) == 4:
            # Sender
            forwardnet_data = argv[1]
            receiver_data = argv[2]
            sender_time = argv[3]
            time_pipe = open("../pipes/{0}".format(sender_time), "w")
            print("time opened")
            sender = open("../pipes/{0}".format(receiver_data), "wb")
            print("sender opened")
            reader = open("../pipes/{0}".format(forwardnet_data), "rb")
            print("reader opened")
            send_read_file(reader, time_pipe, sender)

        elif len(argv) == 3:
            # Receiver
            backwardnet_data = argv[1]
            sender_data = argv[2]
            reader = open("../pipes/{0}".format(backwardnet_data), "rb")
            print("reader opened")
            sender = open("../pipes/{0}".format(sender_data), "wb")
            print("writer opened")
            receive_read_file(reader, sender)
    except BrokenPipeError:
        exit(0)
