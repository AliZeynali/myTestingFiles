import sys
import os
import struct

windowSize = 1
data = []
header = []
SYN = 0
ACK = 0
FIN = 0
senderPort = 0
receiverPort = 0
firstSequenceNumber = 0
lastRecievedData = -1


def writeAckonBackwardNet(packet):
    l = len(packet)
    x0 = l % 256
    l = l // 256
    x1 = l % 256
    l = l // 256
    x2 = l % 256
    l = l // 256
    x3 = l
    if x3 >= 256:
        raise Exception('Very large buffer')

    with open("../pipes/backwardnet_data.pipe", "wb") as file:
        file.write(bytes([x3, x2, x1, x0]))
        file.write(bytes(packet))
        file.flush()

def readFromReceiverData(receiverDataPipe):
    with open(receiverDataPipe, "rb") as file:
        while True:
            length = file.read(4)
            lengthP = ((length[0] * 256 + length[1]) * 256 + length[2]) * 256 + length[3]
            if lengthP > 0:
                packet = file.read(lengthP)
                return packet
            else:
                continue

def getSequenceNumber():
    return struct.pack("!HH", 0, 0)


def getAcknowledgmentNumber(sequnceNumber): #in receiver
    if ACK == 1:
        result = sequnceNumber + 1
        ack0 = result % (256 * 256)
        result = result // (256 * 256)
        ack1 = result % (256 * 256)
        return struct.pack("!HH", ack1, ack0)
    else:
        return struct.pack("!HH", 0, 0)


def getDataOffsetAndReservedAndNS():
    return struct.pack("!B", (5 << 4))


def getCEUAPRSF(CWR):
    if SYN:
        result = (ACK << 4) + (1 << 1)
    elif FIN:
        result = (ACK << 4) + 1
    elif CWR:
        result = (1 << 7)
    else:
        result = (ACK << 4)
    return struct.pack("!H", result)

def unpackCEUAPRSF(CEUAPRSF):
    CEUAPRSF = "{0:08b}".format(struct.unpack_from("!B", CEUAPRSF)[0])
    return int(CEUAPRSF[0]), int(CEUAPRSF[3]), int(CEUAPRSF[6]), int(CEUAPRSF[7])


def getChecksum(packetWithZero):
    byteList = []
    length = len(packetWithZero)
    index = 0
    if length % 2 == 1:
        packetWithZero += struct.pack("!B", 0)
        length += 1
    while True:
        if index >= length:
            break
        element = packetWithZero[index: index + 2]
        byteList.append(element)
        index += 2
    answer = 0
    for value in byteList:
        ############
        value = struct.unpack_from("!H", value)[0]
        answer += value
        if answer > 2 ** 16:
            answer = answer - 2 ** 16 + 1
    answer = 2 ** 16 - answer
    checkSum = struct.pack("!H", answer)
    # newPacket = pack[0:16] + checkSum + pack[18:]
    ###############
    return checkSum

#todo


def getUrgentPointer():
    return struct.pack("!H", 0)


def unpack(packet):
    return packet[0:20], packet[20:]


def unpackHeader(header):
    global senderPort, receiverPort
    senderPort, receiverPort, sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = \
        header[0:2], header[2:4], header[4:8], header[8:12], header[12], header[13], header[14:16], header[16:18]
    senderPort = int(struct.unpack("!H", senderPort)[0])
    receiverPort = int(struct.unpack("!H", receiverPort)[0])
    return sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum


def packHeader(seqNum, ackNum, dataOffsetAndReservedNS, CEUAPRSF, checksum, urgent):
    return struct.pack("!HH", senderPort,
                       receiverPort) + seqNum + ackNum + dataOffsetAndReservedNS + CEUAPRSF + struct.pack("!H",
                                                                                                          windowSize) + checksum + urgent


if __name__ == "__main__":

    if len(sys.argv) == 2:
        receiverDataPipe = '../pipes/receiver_{0}_data.pipe'.format(sys.argv[1])

        try:
            os.mkfifo(receiverDataPipe)
        except Exception:
            print ("error")
            exit(0)

        receiverPort = sys.argv[1]
        print("receiving host {0}: is running...".format(receiverPort))

        #receive SYN:

        while True:
            SYNHeader = readFromReceiverData(receiverDataPipe)
            firstSequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                SYNHeader)
            if checksum != getChecksum(
                    packHeader(firstSequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack , syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if syn:
                    break

        #send SYN/ACK:
        SYN = 1
        ACK = 1
        writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(firstSequenceNumber),
                                     getDataOffsetAndReservedAndNS(),
                                     getCEUAPRSF(0),
                                     getChecksum(packHeader(getSequenceNumber(),
                                                            getAcknowledgmentNumber(firstSequenceNumber),
                                                            getDataOffsetAndReservedAndNS(),
                                                            getCEUAPRSF(0), struct.pack("!H", 0),
                                                            getUrgentPointer())), getUrgentPointer()))
        SYN = 0
        # receive ACK:

        while True:
            ACKHeader = readFromReceiverData(receiverDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                ACKHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if ack:
                    break


        #receive packets and send ACKs
        while (True):
            header , dataLoad = unpack(readFromReceiverData(receiverDataPipe))
            newSequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                header)
            if checksum != getChecksum(packHeader(newSequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,struct.pack("!H", 0), getUrgentPointer()), getUrgentPointer()):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if fin:
                    break
                elif data[newSequenceNumber - firstSequenceNumber - 2] == None:
                    header[newSequenceNumber - firstSequenceNumber - 2] = header
                    data[newSequenceNumber - firstSequenceNumber - 2] = dataLoad
                    if newSequenceNumber - firstSequenceNumber - 2 == lastRecievedData + 1:
                        writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber), getDataOffsetAndReservedAndNS()
                                                         , getCEUAPRSF(0), getChecksum(packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber), getDataOffsetAndReservedAndNS()
                                                         , getCEUAPRSF(0), struct.pack("!H", 0), getUrgentPointer())), getUrgentPointer()))
                        lastRecievedData += 1
                        while data[lastRecievedData+1] != None :
                            newSequenceNumber += 1
                            writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                           getDataOffsetAndReservedAndNS()
                                           , getCEUAPRSF(0), getChecksum(
                                        packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                                   getDataOffsetAndReservedAndNS()
                                                   , getCEUAPRSF(0), struct.pack("!H", 0), getUrgentPointer())),
                                           getUrgentPointer()))
                            lastRecievedData += 1
                        continue
                else:
                    if newSequenceNumber - firstSequenceNumber - 2 == lastRecievedData + 1:
                        writeAckonBackwardNet(
                            packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                       getDataOffsetAndReservedAndNS()
                                       , getCEUAPRSF(0),
                                       getChecksum(packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                                   getDataOffsetAndReservedAndNS()
                                                   , getCEUAPRSF(0), struct.pack("!H", 0), getUrgentPointer())),
                                       getUrgentPointer()))
                        lastRecievedData += 1
                        while data[lastRecievedData+1] != None :
                            newSequenceNumber += 1
                            writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                           getDataOffsetAndReservedAndNS()
                                           , getCEUAPRSF(0), getChecksum(
                                        packHeader(getSequenceNumber(), getAcknowledgmentNumber(newSequenceNumber),
                                                   getDataOffsetAndReservedAndNS()
                                                   , getCEUAPRSF(0), struct.pack("!H", 0), getUrgentPointer())),
                                           getUrgentPointer()))
                            lastRecievedData += 1
                        continue


        #receive FIN
        while True:
            FINHeader = readFromReceiverData(receiverDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                FINHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if fin:
                    break

        #send FIN/ACK
        FIN = 1
        ACK = 1

        writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(sequenceNumber),
                                         getDataOffsetAndReservedAndNS(),
                                         getCEUAPRSF(0),
                                         getChecksum(packHeader(getSequenceNumber(),
                                                                getAcknowledgmentNumber(sequenceNumber),
                                                                getDataOffsetAndReservedAndNS(),
                                                                getCEUAPRSF(0), struct.pack("!H", 0),
                                                                getUrgentPointer())), getUrgentPointer()))
        sequenceNumber += 1

        #send FIN
        ACK = 0

        writeAckonBackwardNet(packHeader(getSequenceNumber(), getAcknowledgmentNumber(sequenceNumber),
                                         getDataOffsetAndReservedAndNS(),
                                         getCEUAPRSF(0),
                                         getChecksum(packHeader(getSequenceNumber(),
                                                                getAcknowledgmentNumber(sequenceNumber),
                                                                getDataOffsetAndReservedAndNS(),
                                                                getCEUAPRSF(0), struct.pack("!H", 0),
                                                                getUrgentPointer())), getUrgentPointer()))

        #receive FIN/ACK
        while True:
            FINACKHeader = readFromReceiverData(receiverDataPipe)
            sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF, windowSize, checksum = unpackHeader(
                FINACKHeader)
            if checksum != getChecksum(
                    packHeader(sequenceNumber, acknowledgmentNumber, dataOffsetAndReservedAndNS, CEUAAPRSF,
                               struct.pack("!H", 0), struct.pack("!H", 0))):
                continue
            else:
                cwr, ack, syn, fin = unpackCEUAPRSF(CEUAAPRSF)
                if fin and ack:
                    break

        print("receiving host {0}: is terminated.".format(receiverPort))